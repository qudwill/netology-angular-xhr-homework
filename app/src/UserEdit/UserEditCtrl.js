'use strict'

userApp.controller('UserEditCtrl', function ($scope, $routeParams, UsersService) {
  UsersService.getUser($routeParams['userId']).then(function (response) {
    $scope.user = response.data
  })

  $scope.updateUser = function (user) {
    $scope.updateSuccess = false

    UsersService.updateUser(user).then(function (response) {
      $scope.userId = response.data.id
      $scope.updateSuccess = true
    })
  }
})
