'use strict'

userApp.controller('UserListCtrl', function ($scope, UsersService, PostsService, $q) {
  var obj1;
  var obj2;

  $q.all({
    obj1: UsersService.getUsers(),
    obj2: PostsService.getPosts()
  }).then(function(values) {
    $scope.users = values.obj1.data
    $scope.posts = values.obj2.data
  })

  // UsersService.getUsers().then(function (response) {
  //   $scope.users = response.data
  // })

  // PostsService.getPosts().then(function (response) {
  //   $scope.posts = response.data
  // })


/*   UsersService.getUsers().then(function (response) {
    $scope.users = response.data
    return PostsService.getPosts()
  }).then(function (response) {
    $scope.posts = response.data
  }) */

})
